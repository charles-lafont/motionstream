<?php
require('header.php');

require ('controller/bdd.php');
$db = getDatabaseConnexion(); 
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$id = $_GET["id"];
$request = $db->query('SELECT * FROM films WHERE id ='. $id); 



$user = $_SESSION['id']
?>


<div class="informations">

<?php while ($movies = $request->fetchObject()) { ?>



<h1><?= $movies->title ?></h1>

<div class="iframe">
  <iframe width="80%" height="700" src="https://www.youtube.com/embed/<?= $movies->url ?>"" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

  
<div class="movie single-movie">
  <img src="uploads/<?= $movies->cover ?>" alt="Spongebob the movie">
</div>

<h2>Categorie</h2>
<hr>
<p><?= $movies->categorie ?></p>

<h2>Synopsis</h2>
<hr>       
<p><?= $movies->synopsis ?></p>

<h2>Duration</h2>
<hr>
<p><?= $movies->duration ?></p>

<h2>Out</h2>
<hr>
<p><?= $movies->date_out ?></p>

<h2>Actors</h2>
<hr>
<p><?= $movies->actors ?></p>

<h2>Evaluation</h2>
<hr>
<p><?= $movies->note ?></p>

<h2>Age required</h2>
<hr>
<p><?= $movies->age_classification ?></p>
<?php } ?>
</div>
 
<div class="buttons">
<div>
<a href="update.php?id=<?=$id;?>">
<button class="btn btn-warning"> Update </button>
</a>
</div>

<div>
<a href="controller/delete_controller.php?id=<?=$id;?>">
<button class="btn btn-danger"> Delete </button>
</a>
</div>
<div>
<form method="POST" action="controller/fav_controller.php?id=<?=$id;?>">
<button type="submit" class="btn btn-success"> Favorite </button>
</form>
</div>

</div>
<div class="notation">
<form method="POST" action="controller/notation.php?id=<?=$id;?>">
  <label for="notation">Notation</label>
  <input type="number" class="form-control" name="notation" required>
  <button type="submit" class="btn btn-primary"> Send </button>
</div>

 



<?php
require('footer.php')
?>