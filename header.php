<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Disney +</title>
</head>
<body>
  <?php
session_start();
  ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">My Motion Stream</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.php">Home</a>
              </li>
             
              <?php 
                if(!isset($_SESSION['id'])){
              ?>
               <li class="nav-item">
                <a class="nav-link" href="login.php">log In</a>
              </li>

              <?php } else { ?>
                <li class="nav-item">
                <a class="nav-link" href="add.php">Add</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="dashboard.php">Account</a>
              </li>
                <li class="nav-item">
                <a class="nav-link" href="controller/logout.php">log out</a>
              </li>
              <?php } ?>
              <li class="nav-item">
                <a class="nav-link" href="signup.php">Sign Up</a>
              </li>
             
             
              <li class="nav-item">
                <a class="nav-link" href="pricing.php">Pricing</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pricing.php">Marketing</a>
              </li>
            </ul>
            <form class="d-flex">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <a href="controller/bdd.php">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </a>
        
            </form>
          </div>
        </div>
      </nav>