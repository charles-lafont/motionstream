## About this app

Motion Stream is a streaming website where we can stock movies with a sql database.

## Read a movie

Read a movie listed in the database at home page

## Create a movie

Add a movie by using add form 

## Update a movie

Update a movie by using update form by pressing update button in single page of the movie

## Delete a movie

Delete a movie by using delete button in single page of the movie

## Register

Create account to be logged and having access to Crud functions





