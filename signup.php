<?php
require('header.php')
?>

<h2 class="text-center">Sign Up</h2>

<form class="add-movie" method="POST" action="controller/signup_controller.php">
<div class="mb-3">
    <label for="lastname" class="form-label">lastname</label>
    <input type="text" name="lastname" class="form-control" id="lastname" aria-describedby="lastname">
    
  </div>

  <div class="mb-3"> 
    <label for="firstname" class="form-label">Firstname</label>
    <input name="firstname" id="firstname"  class="form-control">Firstname</input>
  </div>
 
  <div class="mb-3">
    <label for="firstname" class="form-label">Email</label>
    <input type="email" name="email" id="email"  class="form-control">Email</input>
  </div>

  <div class="mb-3">
    <label for="adress" class="form-label">Adress</label>
    <input type="text" name="adress" id="adress"  class="form-control">Adress</input>
  </div>

  <div class="mb-3">
    <label for="postal" class="form-label">Postal</label>
    <input type="number" name="postal" id="postal"  class="form-control">Postal</input>
  </div>
 
  <div class="mb-3">
    <label for="city" class="form-label">City</label>
    <input type="text" name="city" id="city"  class="form-control">City</input>
  </div>


  <div class="mb-3">
    <label for="password" class="form-label">Password</label>
    <input type="password" name="password" id="password"  class="form-control">Password</input>
  </div>

  
 
 

  <button type="submit" class="btn btn-primary">Save</button>
</form>

<?php
require('footer.php')
?>