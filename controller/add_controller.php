<?php
require ('bdd.php');
$db = getDatabaseConnexion(); 
 

$title = $_POST['title'];
$categorie = $_POST['categorie'];
$dateout = $_POST['release'];
$actors = $_POST['actors'];
$age = $_POST['age'];
$synopsis = $_POST['synopsis'];
$url = $_POST['url'];


//début du traitement de fichier
$photo = $_FILES["picture"]; // récup le fichier 
$fileName = $_FILES['picture']['name'];
$validExt = array('.jpg', '.jpeg', '.gif', '.png');
$fileExt = "." . strtolower(substr(strrchr($fileName, '.'),1));

if (!in_array($fileExt, $validExt)) {
    echo 'wrong format';
    die;
}

$tmpName = $_FILES['picture']['tmp_name'];
$uniqueName = md5(uniqid(rand(), true));
$fileName = "../uploads/" . $uniqueName . $fileExt;
$resultat = move_uploaded_file($tmpName, $fileName);
$sql = "INSERT INTO `films` (title, categorie, synopsis, actors, age_classification, cover, url) VALUES (:title, :categorie, :synopsis, :actors, :age, :cover, :url)";

$query = $db->prepare($sql);


$query->bindValue(':title', $title, PDO::PARAM_STR);
$query->bindValue(':categorie', $categorie, PDO::PARAM_STR);
$query->bindValue(':synopsis', $synopsis, PDO::PARAM_STR);
$query->bindValue(':actors', $actors, PDO::PARAM_STR);
$query->bindValue(':age', $age, PDO::PARAM_INT);
//$query->bindValue(':dateout', NOW(), PDO::PARAM_INT);
$query->bindValue(':cover', $fileName, PDO::PARAM_STR);
$query->bindValue(':url', $url, PDO::PARAM_STR);


$query->execute();

header('location:../Index.php');

?>



