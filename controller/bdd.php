<?php function getDatabaseConnexion() {
        
        // Paramètre de connexion serveur
        $host = "localhost:8888";
        $login= "root";     // identifiant d'accès au serveur 
        $password="root";    // mot de passe pour s'identifier au serveur
        $base = "motionstream";    // la base de données
      
        try 
        {
                 
         $pdo = new PDO('mysql:host=localhost;dbname=motionstream;charset=utf8', 'root', 'root'); // connexion à la base de donnée, une fois la connexion établie, $db devient un objet
             return $pdo; // la connexion établie, :$db permet d'utiliser toutes les méthodes de la PDO (query(), prepare(), execute() . . .)
             echo'connected';
         } 
         catch (Exception $e) 
         {
             echo 'Erreur : ' . $e->getMessage() . '<br>';
             echo 'N° : ' . $e->getCode() . '<br>';
             die('Connexion au serveur impossible.');
         } 
     
    } ?>
