<?php
require('header.php');

require ('controller/bdd.php');
$db = getDatabaseConnexion(); 
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$requete = "SELECT * FROM categories";

$result = $db->query($requete); //Exécute une requête SQL, retourne un jeu de résultats en tant qu'objet PDOStatement

?>

<h2 class="text-center">Add a movie</h2>

<form class="add-movie" method="POST" action="controller/add_controller.php" enctype="multipart/form-data">
<div class="mb-3">
    <label for="title" class="form-label">Title</label>
    <input type="text" class="form-control" id="title" name="title" aria-describedby="title">
    
  </div>
  <div class="mb-3">
    <label for="categorie" class="form-label">Categorie</label>
   
    <select name="categorie" class="form-control">
    <?php while ($cat = $result->fetchObject()){ ?>
 
 <option value= "<?php echo $cat->name; ?>" ><?php echo $cat->name; ?></option>

    <?php } ?>
    </select>
  </div>
  <div class="mb-3">
    <label for="synopsis" class="form-label">Synopsis</label>
    <textarea name="synopsis" id="synopsis" cols="30" rows="10" class="form-control"></textarea>
  </div>

  <div class="mb-3">
    <label for="release" class="form-label">Release</label>
    <input type="date" class="form-control" id="release" name="release" aria-describedby="release">

  </div>

  <div class="mb-3">
    <label for="cover" class="form-label">Cover</label>
    <input type="file" class="form-control"  name="picture" required>
  </div>

  <div class="mb-3">
    <label for="actors" class="form-label">actors</label>
    <input type="text" class="form-control" id="actors" name="actors" aria-describedby="actors">
  </div>

  <div class="mb-3">
    <label for="age" class="form-label">Age classification</label>
    <input type="number" class="form-control" id="age" name="age" aria-describedby="age">
  </div>

  <div class="mb-3">
    <label for="video" class="form-label">Video</label>
    <input type="text" class="form-control" id="video" name="url" aria-describedby="video">
  </div>


  <input type="submit" class="btn btn-primary">Save</button>
</form>

<?php
require('footer.php')
?>